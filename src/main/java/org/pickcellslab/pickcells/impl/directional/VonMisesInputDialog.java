package org.pickcellslab.pickcells.impl.directional;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.pickcellslab.pickcells.api.app.directional.VonMisesOptions;

@SuppressWarnings("serial")
public class VonMisesInputDialog extends JDialog {

	private boolean wasCancelled = true;
	private VonMisesOptions options;

	public VonMisesInputDialog() {

		JLabel lblMinNumberOf = new JLabel("Min Number of Distributions");

		JFormattedTextField minField = new JFormattedTextField();
		minField.setText("1");

		JLabel lblMaxNumberOf = new JLabel("Max Number of Distributions");

		JFormattedTextField maxField = new JFormattedTextField();
		maxField.setText("1");

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{
			
			try{
			int min = Integer.parseInt(minField.getText());
			int max = Integer.parseInt(maxField.getText());
			if(max<min){
				JOptionPane.showMessageDialog(null, "Max cannot be smaller than min");
				return;
			}
			options = new VonMisesOptions(min,max);
			}catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null, "One of your input is not a valid number");
				return;
			}				
			wasCancelled = false;
			this.dispose();
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->this.dispose());







		//Layout
		//------------------------------------------------------------------------------------------


		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblMinNumberOf)
								.addComponent(lblMaxNumberOf, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addComponent(maxField, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
										.addComponent(minField, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)))
										.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
												.addContainerGap(119, Short.MAX_VALUE)
												.addComponent(btnOk)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(btnCancel)
												.addContainerGap())
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblMinNumberOf)
								.addComponent(minField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(lblMaxNumberOf)
										.addComponent(maxField, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
												.addComponent(btnOk)
												.addComponent(btnCancel))
												.addContainerGap())
				);
		getContentPane().setLayout(groupLayout);
	}



	public boolean wasCancelled(){
		return wasCancelled;
	}

	public VonMisesOptions get(){
		return options;
	}

}
