package org.pickcellslab.pickcells.impl.directional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPDouble;
import org.rosuda.REngine.REXPGenericVector;
import org.rosuda.REngine.REXPList;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REXPString;
import org.rosuda.REngine.REngine;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.REngineStdOutput;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.JRI.JRIEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;





/**
 * This class gets a connection to R and allows to work with the movMF package to fit mixtures of von Mises Distributions
 * <a href="http://www.jstatsoft.org/v58/i10/">http://www.jstatsoft.org/v58/i10/</a>
 *
 * @author Guillaume Blin
 *
 */
public class MovMF {


	//Logging
	private static Logger log = LoggerFactory.getLogger(MovMF.class);


	//R engine
	private static REngine engine;


	//Output of the analysis
	private int modes;
	private RList bestFit;


	/**
	 * Creates a new MovMF object with a reference to a static R session. 
	 * This constructor uses pre defined parameters to create an instance of movMF in R
	 * @param theta A Matrix theta defined as k*mu where k is the concentration parameter and mu the mean direction.
	 * @param alpha The weight of each distribution in the mixture
	 * @param L The value of Bayesian information criterion for the current fit
	 * @throws REngineException
	 */
	public MovMF(double[][] theta, double[] alpha, double L) throws REngineException{

		//Connect to R
		engine = JRIEngine.getLastEngine();
		if(engine == null)
			engine = JRIEngine.createEngine(new String[]{"--no-save"},new REngineStdOutput(),true);

		//load movMF
		try {
			engine.parseAndEval("library(\"movMF\")");
		} catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Create a movMF REXP and assign in R
		bestFit = new RList();
		bestFit.put("theta", REXP.createDoubleMatrix(theta));
		bestFit.put("alpha", new REXPDouble(alpha));
		bestFit.put("L", new REXPDouble(new double[]{L}));


		try {
			REXP vMF = new REXPGenericVector(bestFit,
					new REXPList(
							new RList(
									new REXP[] {
											new REXPString("movMF"),
											new REXPString(bestFit.keys())},
											new String[] {
											"class",
											"names"
									})));
			engine.assign("bestMF", vMF);
		} catch (REXPMismatchException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 

	}





	/**
	 * Creates a new MovMF object with a reference to a static R session. 
	 * This constructor takes cartesian coordinates of points on a sphere and finds the best
	 * mixtures of von Mises-Fisher distributions using the MovMF package from R.
	 * @param x
	 * @param y
	 * @param z
	 * @param kmin The minimum number of mixtures to fit the distribution
	 * @param kmax The maximum number of mixtures to fit the distribution
	 * @throws REngineException 
	 */
	public MovMF(List<double[]> vectors, int kmin, int kmax) throws REngineException{

		//Check vectors
		//First remove all null or NaN values
		List<double[]> filtered = vectors.stream().filter(d -> d!= null && !Double.isNaN(d[0])).collect(Collectors.toList());
		int numDimensions = filtered.get(0).length;



		if(filtered.isEmpty())
			throw new IllegalArgumentException("The provided list of vectors is empty or the list only contains null or NaN values");



		//Connect to R
		engine = JRIEngine.getLastEngine();
		if(engine == null)
			engine = JRIEngine.createEngine(new String[]{"--no-save"},new REngineStdOutput(),true);

		//load movMF

		try {
			engine.parseAndEval("library(\"movMF\")");

			//create matrix
			double[][] array = filtered.toArray(new double[numDimensions][filtered.size()]);
			//for(int i = 0; i<array.length;i++)
			//	System.out.println(Arrays.toString(array[i]));

			REXP data = REXP.createDoubleMatrix(array);
			engine.assign("xyz", data);


			//engine.parseAndEval("xyz <- cbind(x,y,z)");
			//engine.parseAndEval("m <- as.matrix(xyz)");

			//Create fits with kmin to kmax
			engine.parseAndEval("vMFs <- lapply("+kmin+":"+kmax+", function(K) movMF(xyz, k = K, control= list(nruns = 50)))");

			log.debug("fits done in R");

			//Compare BIC values for the different mixtures
			double[] bics = engine.parseAndEval("sapply(vMFs, BIC)").asDoubles();
			int maxIndex = 0;
			for (int i = 1; i <bics.length; i++){
				double current = bics[i];
				//log.debug(current);
				if ((current < bics[maxIndex])){
					maxIndex = i;
				}
			}    
			modes = maxIndex+1;

			log.debug("Best fit found with k = "+modes);

			bestFit = engine.parseAndEval("bestMF <- vMFs[["+modes+"]]").asList();

		} catch (REXPMismatchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(Arrays.toString(bestFit.keys()));
	}



	@Override
	public void finalize(){
		engine.close();
		System.out.println("R Engine closed");
		bestFit = null;
	}





	/**
	 * @return the number of mixtures in the current model
	 */
	public int getModes(){
		return modes;
	}

	/**
	 * A Matrix theta defined as k*mu where k is the concentration parameter and mu the mean direction.
	 * Thus k may be obtained for each distribution using the norm of each column vector
	 * @return The theta matrix
	 */
	public double[][] theta(){
		try {
			return bestFit.at("theta").asDoubleMatrix();
		} catch (REXPMismatchException e) {
			e.printStackTrace();
			//should not happen
			return null;
		}
	}


	public double[] probabilities(){
		try {
			return bestFit.at("alpha").asDoubles();
		}  catch (REXPMismatchException e) {
			e.printStackTrace();
			//should not happen
			return null;
		}
	}

	/**
	 * @return the value of Bayesian information criterion for the current fit
	 */
	public double bic(){
		try {
			return bestFit.at("L").asDouble();
		} catch (REXPMismatchException e) {
			e.printStackTrace();
			//should not happen
			return 0;
		}
	}




	public int[] predict(double[][]x){
		if(bestFit == null)
			throw new IllegalStateException("No memberships can be defined before a fit has bee created");		


		try {

			//Load data into R
			engine.assign("xyz", REXP.createDoubleMatrix(x));



			return engine.parseAndEval("predict(bestMF,xyz)").asIntegers();
		} catch (REXPMismatchException | REngineException e) {
			e.printStackTrace();
			return null;
		}
	}



	public int predict(double[] ds) {
		return predict(new double[][]{ds})[0];
	}



	public double[][] getMemberships(double[][] x){

		if(bestFit == null)
			throw new IllegalStateException("No memberships can be defined before a fit has bee created");		


		double[][] memberships = null;
		try {

			//Load data into R
			engine.assign("xyz", REXP.createDoubleMatrix(x));

			memberships = engine.parseAndEval("predict(bestMF, xyz, type = \"memberships\")").asDoubleMatrix();

		} catch (REXPMismatchException | REngineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return memberships;

	}




	public double[] getMemberships(double[] ds) {
		return getMemberships(new double[][]{ds})[0];
	}




	/**
	 * This method correspond to the dmovMF function in the package, alpha and theta taken form the best fit.
	 * findBestFit() must have been called previously.
	 * @param log specifies whether or not to return the log of the values
	 * @return An array of length x.length corresponding to the density values for the specified coordinates
	 */
	public double[] getDensities(double[][] x, boolean log)throws IllegalStateException{


		double[] densities = null;
		try{
			//Load data into R
			engine.assign("xyz", REXP.createDoubleMatrix(x));
			String makeLog = "F";
			if(log)makeLog = "T";
			// engine.parseAndEval("m <- as.matrix(xyz)");

			densities = engine.parseAndEval("dmovMF(xyz, bestMF$theta,bestMF$alpha,"+makeLog+")").asDoubles();
		}catch(NullPointerException | REngineException | REXPMismatchException e){
			throw new IllegalStateException("findBestFit() must be called and return before calling this getDensities");
		}

		return densities;

	}

	public double getDensity(double[] x){
		return getDensities(new double[][]{x}, false)[0];
	}



	public enum Fit{
		X(0),Y(1),Z(2),D(3);    
		private final int id;
		Fit(int id) { this.id = id; }
		public int getValue() { return id; }
	}

	/**
	 * Generates a list of points uniformly distributed around the sphere with associated densities
	 * Use the Fit enum to access the different column values
	 */
	public List<double[]> getBestFitPoints(int n){

		//TODO exceptions

		double size = n;
		List<double[]> points = new ArrayList<>(n);

		//Uniform distribution on the sphere check: 
		//http://www.softimageblog.com/archives/115
		double golden_angle = Math.PI * (3 - Math.sqrt(5));
		System.out.println("golden = "+golden_angle);
		double off = 2/size;


		final int x = 0, y = 1, z = 2, d = 4;


		for (int k = 0; k<size; k++){

			double[] c = new double[4];

			c[z] = k*off-1 + (off/2);
			double r1 = Math.sqrt(1-c[z]*c[z]);
			double phi = k*golden_angle;

			c[x] = Math.cos(phi)*r1;
			c[y] = Math.sin(phi)*r1;


			c[d] = getDensity(new double[]{c[x],c[y],c[z]});

			points.add(c);


			/**
        Spherical coordinates
        double r2 = Math.sqrt(Math.pow(x[k],2)+Math.pow(y[k],2)+Math.pow(z[k],2));
        System.out.println(r2);
        double p = Math.acos(z[k]/r2);       

        double t = Math.atan2(y[k], x[k])+Math.PI;
			 */


		}

		return points;

	}


	/**
	 * @param args
	 * @throws REngineException 
	 */
	public static void main(String[] args) throws REngineException {



		System.out.println(System.getProperty("java.library.path"));

		//1- generate a 2d gaussian distribution 
		int size = 500;
		Random r = new Random();
		List<double[]> data = new ArrayList<>(size);
		for(int i = 0; i<size; i++){
			double[] x = new double[3];
			x[0] = r.nextGaussian();
			x[1] = r.nextGaussian();
			x[2] = r.nextGaussian();
			data.add(x);
		}

		MovMF mmf = new MovMF(data,3, 3);

		System.out.println("Bic : "+ mmf.bic());
		System.out.println("theta : "+ Arrays.toString(mmf.theta()));
		System.out.println("Probabilities : "+ Arrays.toString(mmf.probabilities()));
		System.out.println("memberships : "+ Arrays.toString(mmf.getMemberships(data.get(250))));
		System.out.println("predict : "+ mmf.predict(data.get(250)));

		MovMF mmf2 = new MovMF(mmf.theta(),mmf.probabilities(),mmf.bic());

		System.out.println("Bic : "+ mmf2.bic());
		System.out.println("theta : "+ Arrays.toString(mmf2.theta()));
		System.out.println("Probabilities : "+ Arrays.toString(mmf2.probabilities()));
		System.out.println("memberships : "+ Arrays.toString(mmf2.getMemberships(data.get(250))));
		System.out.println("predict : "+ mmf2.predict(data.get(250)));

	}



}