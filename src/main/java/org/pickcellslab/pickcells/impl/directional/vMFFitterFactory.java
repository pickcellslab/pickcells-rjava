package org.pickcellslab.pickcells.impl.directional;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.pickcells.api.app.directional.DirectionalFittingMethod;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistributionFitter;
import org.pickcellslab.pickcells.api.app.directional.VonMisesFitterFactory;
import org.pickcellslab.pickcells.api.app.directional.VonMisesMixture;
import org.pickcellslab.pickcells.api.app.directional.VonMisesOptions;

@Module
public class vMFFitterFactory implements VonMisesFitterFactory {

	private static final DirectionalFittingMethod<VonMisesOptions> method = new vMFMethod();
	
	@Override
	public DirectionalFittingMethod<VonMisesOptions> method() {
		return method;
	}

	@Override
	public DirectionalRealMultiVariateDistributionFitter<VonMisesOptions, VonMisesMixture> createFitter() {
		return new vMFFitter();
	}

	@Override
	public String group() {
		return "Directional Statistics";
	}

}
