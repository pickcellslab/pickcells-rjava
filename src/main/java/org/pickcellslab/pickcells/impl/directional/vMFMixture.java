package org.pickcellslab.pickcells.impl.directional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.pickcellslab.foundationj.annotations.Data;
import org.pickcellslab.foundationj.annotations.WithService;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistribution;
import org.pickcellslab.pickcells.api.app.directional.VonMisesMixture;
import org.rosuda.REngine.REngineException;


/**
 * A {@link NodeItem} which allows to persist a mixture of von Mises Distribution
 * @author Guillaume Blin
 *
 */


@Data(typeId="von Mises-Fisher Mixture")
@WithService(DataAccess.class)
public class vMFMixture extends DataNode implements VonMisesMixture{


	private static final List<AKey<?>> min = new ArrayList<>();	
	private static AKey<Integer> d = AKey.get("num dims",Integer.class);

	static{
		min.add(k); min.add(theta); min.add(bic);
		min.add(WritableDataItem.idKey); min.add(proba); min.add(info);
	}



	private MovMF mmf;//For now we depend on R	


	@SuppressWarnings("unused")
	private vMFMixture(){
		//For the database
	}


	/**
	 * Creates a new Mixture of Von Mises Distribution using the provided dataset to fit and documented using the
	 * provided information. Note that the vectors must be normalized
	 * @param name The name to associate to this instance
	 * @param vectors The list of vectors coordinates (cartesian)
	 * @param kMin 
	 * @param kMax
	 * @throws REngineException
	 */
	public vMFMixture(List<double[]> vectors, int kMin, int kMax) throws REngineException{

		//filter out null vectors

		vectors = vectors.stream().filter(d->d!=null).collect(Collectors.toList());

		mmf = new MovMF(vectors, kMin, kMax);

		//set attributes
		double[] p = mmf.probabilities();
		setAttribute(k,p.length);	
		setAttribute(proba,p);		
		setAttribute(bic,mmf.bic());
		setAttribute(d,vectors.get(0).length);

		//multidimensional array must be converted	
		setAttribute(theta,AKey.to1dArray(mmf.theta()));

	}




	@Override
	public int numDistributions() {
		return getAttribute(k).get();
	}

	@Override
	public double[] weights() {
		return getAttribute(proba).get();
	}

	@Override
	public Distribution<double[]>[] distributions() {
		throw new RuntimeException("Not yet implemented"); //TODO return appropriate values when R dependency is removed
	}

	@Override
	public int[] predictions(double[][] coordinates) {
		if(mmf==null)
			try {
				mmf = new MovMF(
						AKey.restoreMatrix(getAttribute(theta).get(),getAttribute(k).get()),
						getAttribute(proba).get(),
						getAttribute(bic).get());
			} catch (REngineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return mmf.predict(coordinates);
	}


	@Override
	public int predictions(double[] coordinate) {
		if(mmf==null)
			try {
				mmf = new MovMF(
						AKey.restoreMatrix(getAttribute(theta).get(),getAttribute(k).get()),
						getAttribute(proba).get(),
						getAttribute(bic).get());
			} catch (REngineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return mmf.predict(coordinate);
	}



	@Override
	public double[][] memberships(double[][] coordinates) {
		if(mmf==null)
			try {
				mmf = new MovMF(
						AKey.restoreMatrix(getAttribute(theta).get(),getAttribute(k).get()),
						getAttribute(proba).get(),
						getAttribute(bic).get());
			} catch (REngineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return mmf.getMemberships(coordinates);
	}



	@Override
	public double[] memberships(double[] coordinate) {
		return mmf.getMemberships(coordinate);
	}


	@Override
	public Stream<AKey<?>> minimal() {
		return min.stream();
	}

	@Override
	public String toString() {
		return "vonMisesFisher Distribution";
	}


	@Override
	public double[] densities(double[][] coordinates) {
		if(mmf==null)
			try {
				mmf = new MovMF(
						AKey.restoreMatrix(getAttribute(theta).get(),getAttribute(k).get()),
						getAttribute(proba).get(),
						getAttribute(bic).get());
			} catch (REngineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return mmf.getDensities(coordinates, false);
	}





	@Override
	public double density(double[] coordinate) {
		if(mmf==null)
			try {
				mmf = new MovMF(
						AKey.restoreMatrix(getAttribute(theta).get(),getAttribute(k).get()),
						getAttribute(proba).get(),
						getAttribute(bic).get());
			} catch (REngineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return mmf.getDensity(coordinate);
	}



	@Override
	public void setInfo(String info) {
		setAttribute(DirectionalRealMultiVariateDistribution.info,info);
	}


	@Override
	public String getDescription() {

		//Build a description with info + mu angles and kappa for each component

		//Note for each component of the mixture the given vector provides
		//mu = theta/||theta||
		//k = ||theta||

		StringBuilder b = new StringBuilder("Von Mises Mixture Model \n");
		getAttribute(info).ifPresent(i->b.append(i));
		b.append("\n Dimensionality : "+getAttribute(d).get());

		if(getAttribute(d).get() == 3){
			int numMix = getAttribute(k).get();
			double[] k = new double[numMix];
			double[] alpha = new double[numMix];
			double[] thet = new double[numMix];

			double[][] params = AKey.restoreMatrix(getAttribute(theta).get(), numMix);
			for(int i = 0; i<numMix; i++){
				Vector3D v = new Vector3D(params[i]);
				k[i] = v.getNorm();
				alpha[i] = v.getAlpha();
				thet[i] = v.getDelta();
			}
			b.append("\n Kappas : "+Arrays.toString(k));
			b.append("\n mu azimuth : "+Arrays.toString(alpha));
			b.append("\n mu elevation : "+Arrays.toString(thet));
		}

		return	b.toString();
	}


	@Override
	public int dimensions() {
		return getAttribute(d).get();
	}


	@Override
	public String summary() {
		StringBuilder b = new StringBuilder("<HTML>Von Mises Mixture Model ");
		getAttribute(info).ifPresent(i->b.append("<br>"+i));
		b.append("<br> Dimensionality : "+getAttribute(d).get());

		if(getAttribute(d).get() == 3){
			int numMix = getAttribute(k).get();
			double[] k = new double[numMix];
			double[] alpha = new double[numMix];
			double[] thet = new double[numMix];

			double[][] params = AKey.restoreMatrix(getAttribute(theta).get(), numMix);
			for(int i = 0; i<numMix; i++){
				Vector3D v = new Vector3D(params[i]);
				k[i] = v.getNorm();
				alpha[i] = v.getAlpha();
				thet[i] = v.getDelta();
			}
			b.append("<br> Kappas : "+Arrays.toString(k));
			b.append("<br> mu azimuth : "+Arrays.toString(alpha));
			b.append("<br> mu elevation : "+Arrays.toString(thet)+"</HTML>");
		}

		return b.toString();
	}


}
