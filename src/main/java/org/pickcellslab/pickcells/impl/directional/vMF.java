package org.pickcellslab.pickcells.impl.directional;

import java.util.stream.Stream;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.pickcells.api.app.directional.VonMisesDistribution;

/**
 * 
 * Not yet implemented
 *
 */
public class vMF extends DataNode implements VonMisesDistribution {


	private final double k;
	private final int p;
	


	public vMF(double[] mu, double kappa){
		
		if(kappa<=0)
			throw new IllegalArgumentException("kappa must be positive");
		
		k = kappa;
		p = mu.length+1;
	}


	@Override
	public int dimensions() {
		return 3;
	}

	@Override
	public double density(double[] coordinates) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public Stream<AKey<?>> minimal() {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void setInfo(String info) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String summary() {
		// TODO Auto-generated method stub
		return null;
	}



}
