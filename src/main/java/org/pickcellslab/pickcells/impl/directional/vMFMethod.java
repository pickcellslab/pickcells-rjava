package org.pickcellslab.pickcells.impl.directional;

import java.util.Optional;

import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.pickcells.api.app.directional.VonMisesMethod;
import org.pickcellslab.pickcells.api.app.directional.VonMisesOptions;

public class vMFMethod extends VonMisesMethod {

	@Override
	public Optional<FitConfig<VonMisesOptions>> config() {
		VonMisesInputDialog dialog = new VonMisesInputDialog();
		dialog.setModal(true);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		dialog.setVisible(true);
		if(!dialog.wasCancelled())
			return Optional.of(new FitConfig<VonMisesOptions>(this, dialog.get()));
		else return Optional.empty();
	}

	@Override
	public String getName() {
		return "Mixture of vonMises Distribution";
	}

	@Override
	public String getDescription() {
		return "Connects to R and uses the movMF package to fit mixtures of von Mises Distributions"
				+ " <a href=\"http://www.jstatsoft.org/v58/i10/\">http://www.jstatsoft.org/v58/i10/</a>";
	}

	@Override
	public int minNumberOfDimensions() {
		return 1;
	}

	@Override
	public int maxNumberOfDimensions() {
		return Integer.MAX_VALUE;
	}

}
