package org.pickcellslab.pickcells.impl.directional;

import java.util.List;

import org.pickcellslab.foundationj.dataviews.fitting.FitConfig;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistributionFitter;
import org.pickcellslab.pickcells.api.app.directional.VonMisesFitter;
import org.pickcellslab.pickcells.api.app.directional.VonMisesMixture;
import org.pickcellslab.pickcells.api.app.directional.VonMisesOptions;
import org.rosuda.REngine.REngineException;


public class vMFFitter implements VonMisesFitter, DirectionalRealMultiVariateDistributionFitter<VonMisesOptions, VonMisesMixture> {

	

	private String last;


	@Override
	public VonMisesMixture fit(List<double[]> data, FitConfig<VonMisesOptions> config) {
		try {
			
			VonMisesOptions options = config.options();
			
			vMFMixture vMF = new vMFMixture(data,options.kMin(),options.kMax());
			
			last = vMF.getDescription();
			
			return vMF;
		} catch (REngineException e) {
			throw new RuntimeException(e); //TODO make a checked FittingException
		}
	}


	@Override
	public double[][] sample(VonMisesMixture distribution, int size) {
		//TODO use R function to obtain the required sample
		throw new RuntimeException("Not implemented Yet!");
	}


	@Override
	public String lastFitInfos() {
		return last;
	}



}
