# README #

This repository contains PickCells modules which connect to R in order to provide certain functionalities.

## Installation ##

The modules of this repository require [R](https://www.r-project.org/) to be installed as well as [rJava](https://cran.r-project.org/web/packages/rJava/index.html) .

Additional R packages may as well be required by individual PickCells module:
Currently [movMF](https://cran.r-project.org/web/packages/movMF/index.html) is required by the vonmises module.

For rJava to work properly with PickCells, environment variables must be properly set:

- R_HOME must be properly set (NB type R.home() in R to determine the location of R_HOME on your computer). Sometimes R_SHARE_DIR and R_INCLUDE_DIR must also be set.
- rJava jar files must be in the java.library.path
- Additional R packages must be installed globally